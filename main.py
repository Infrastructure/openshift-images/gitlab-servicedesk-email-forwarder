import email
import email.generator
import email.mime
import gzip
import json
import os
import shutil
import smtplib
import sys
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

from imapclient import IMAPClient


def extract_address(header: str) -> str:
    if "<" in header and ">" in header:
        return header.split("<")[-1].split(">")[0]

    return header


def mail_to_multipart(msg):
    if msg.is_multipart():
        return msg

    mail_new = MIMEMultipart("mixed")
    headers = [
        (k, v)
        for (k, v) in msg.items()
        if k not in ("Content-Type", "Content-Transfer-Encoding")
    ]

    for k, v in headers:
        mail_new[k] = v

    for k, v in headers:
        del msg[k]

    mail_new.attach(msg)
    return mail_new


def main():
    IMAP_HOST = os.environ.get("IMAP_HOST")
    IMAP_USER = os.environ.get("IMAP_USER")
    IMAP_PASS = os.environ.get("IMAP_PASS")

    SMTP_HOST = os.environ.get("SMTP_HOST")
    SMTP_PORT = os.environ.get("SMTP_PORT")

    if not all([IMAP_HOST, IMAP_USER, IMAP_PASS, SMTP_HOST, SMTP_PORT]):
        print("ERROR: missing required environment variables", file=sys.stderr)
        sys.exit(1)

    with open("mapping.json") as mapping_file:
        mapping = json.load(mapping_file)

    with IMAPClient(IMAP_HOST) as imap_client:
        imap_client.login(IMAP_USER, IMAP_PASS)
        imap_client.select_folder("INBOX")

        messages = imap_client.search(["UNSEEN"])
        for uid, message_data in imap_client.fetch(messages, "RFC822").items():
            print(f"=> Processing message {uid}")
            message = email.message_from_bytes(message_data[b"RFC822"])
            dest = None

            email_to_headers = message.get_all("To", [])
            email_to = [extract_address(to) for to in email_to_headers]
            if any(to in mapping for to in email_to):
                dest = [to for to in email_to if to in mapping][0]
                print(f"Found destination in To headers: {dest}")

            email_cc_headers = message.get_all("Cc", [])
            email_cc = [extract_address(cc) for cc in email_cc_headers]
            if any(cc in mapping for cc in email_cc):
                dest = [cc for cc in email_cc if cc in mapping][0]
                print(f"Found destination in Cc headers: {dest}")

            if dest:
                imap_client.add_flags(uid, [b"\\Seen"])
                dest = mapping[dest]
                message_id = message["Message-ID"].lstrip("<").rstrip(">")
                message = mail_to_multipart(message)

                with open(f"{message_id}.eml", "wb") as eml_file:
                    generator = email.generator.BytesGenerator(eml_file)
                    generator.flatten(message)

                with open(f"{message_id}.eml", "rb") as eml_file:
                    with gzip.open(f"{message_id}.eml.gz", "wb") as eml_file_gz:
                        shutil.copyfileobj(eml_file, eml_file_gz)

                with open(f"{message_id}.eml.gz", "rb") as eml_file_gz:
                    part = MIMEBase("application", "gzip")
                    part.set_payload(eml_file_gz.read())

                encoders.encode_base64(part)
                part.add_header(
                    "Content-Disposition",
                    "attachment; filename=original_message.eml.gz",
                )
                message.attach(part)

                print(f"Sending message to {dest}")
                with smtplib.SMTP(SMTP_HOST, port=SMTP_PORT) as smtp_client:
                    smtp_client.sendmail(message["From"], dest, message.as_bytes())


if __name__ == "__main__":
    main()
