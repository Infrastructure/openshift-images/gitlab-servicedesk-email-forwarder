FROM registry.access.redhat.com/ubi9/python-311:1
ENV PYTHONPATH=/opt/app-root/src

RUN pip install imapclient
ADD main.py /opt/app-root/src
ADD mapping.json /opt/app-root/src

ENTRYPOINT ["python", "main.py"]
